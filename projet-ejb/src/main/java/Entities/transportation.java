package Entities;

import java.io.Serializable;
import java.lang.Integer;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: transportation
 *
 */
@Entity

public class transportation implements Serializable {

	   
	@Id
	private Integer id;
	private Date Date_arrivee;
	private static final long serialVersionUID = 1L;
	@OneToOne
	@JoinColumn(name="id_driver")
	private driver driver;
	@OneToOne
	@JoinColumn(name="id_truck")
	private truck truck;
	@OneToOne
	@JoinColumn(name="id_order")
	private order order;
	@OneToOne
	@JoinColumn(name="id_finance")
	private finance cout_transportation;
	
	
	public driver getDriver(){
		return driver;
	}
	public truck getTruck(){
		return truck;
	}
	public order getOrder(){
		return order;
	}
	public finance getCout(){
		return cout_transportation;
	}
	

	public transportation() {
		super();
	}   
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}   
	public Date getDate_arrivee() {
		return this.Date_arrivee;
	}

	public void setDate_arrivee(Date Date_arrivee) {
		this.Date_arrivee = Date_arrivee;
	}
   
}
