package Entities;

import java.io.Serializable;
import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: truck
 *
 */
@Entity

public class truck implements Serializable {

	   
	@Id
	private Integer id_truck;
	private int Registration;
	private String Disponibility;
	private Float Capacity;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name="id_user", referencedColumnName="id_user" )
	private user user;
	@OneToOne
	private depence depence;

	public truck() {
		super();
	}   
	public depence getDepence(){
		return depence;
	}
	public user getUser(){
		return user;
	}
	public Integer getId_truck() {
		return this.id_truck;
	}

	public void setId_truck(Integer id) {
		this.id_truck = id;
	}   
	public int getRegistration() {
		return this.Registration;
	}

	public void setRegistration(int Registration) {
		this.Registration = Registration;
	}   
	public String getDisponibility() {
		return this.Disponibility;
	}

	public void setDisponibility(String Disponibility) {
		this.Disponibility = Disponibility;
	}   
	public Float getCapacity() {
		return this.Capacity;
	}

	public void setCapacity(Float Capacity) {
		this.Capacity = Capacity;
	}
   
}
