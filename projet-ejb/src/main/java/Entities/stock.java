package Entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: stock
 *
 */
@Entity

public class stock implements Serializable {

	@Id
	private int id_stock;
	private int numberpro;
	private float totalprice;
	private static final long serialVersionUID = 1L;
	@OneToOne
	@JoinColumn(name="id_station")
	private station station;
	
	@ManyToMany
	@JoinTable(name="stock_produit", joinColumns=@JoinColumn(name="id_stock"), inverseJoinColumns=@JoinColumn(name="id_product"))
	private List<product> products;
	
	public station getStation(){
		return station;
	}

	public stock() {
		super();
	}   
	public int getId_stock() {
		return this.id_stock;
	}

	public void setId_stock(int id_stock) {
		this.id_stock = id_stock;
	}   
	public int getNumberpro() {
		return this.numberpro;
	}

	public void setNumberpro(int numberpro) {
		this.numberpro = numberpro;
	}   
	public float getTotalprice() {
		return this.totalprice;
	}

	public void setTotalprice(float totalprice) {
		this.totalprice = totalprice;
	}
   
}
