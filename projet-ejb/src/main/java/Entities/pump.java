package Entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: pump
 *
 */
@Entity

public class pump implements Serializable {

	@Id
	private int id_pump;
	private float quantity;
	private float Price_fuel;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name="id_user", referencedColumnName="id_user" )
	private user user;
	
	@OneToOne
	@JoinColumn(name="id_finance")
	private finance finance;
	
	@OneToMany(mappedBy="pump")
	private List<delivery> deleveries;

	public pump() {
		super();
	}   
	public int getId_pump() {
		return this.id_pump;
	}

	public void setId_pump(int id_pump) {
		this.id_pump = id_pump;
	}   
	public float getQuantity() {
		return this.quantity;
	}

	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}   
	public float getPrice_fuel() {
		return this.Price_fuel;
	}

	public void setPrice_fuel(float Price_fuel) {
		this.Price_fuel = Price_fuel;
	}
   
}
