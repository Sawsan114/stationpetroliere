package Entities;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: skill
 *
 */
@Entity

public class skill implements Serializable {

	@Id
	private int id_skill;
	private String name;
	private static final long serialVersionUID = 1L;

	public skill() {
		super();
	}   
	public int getId_skill() {
		return this.id_skill;
	}

	public void setId_skill(int id_skill) {
		this.id_skill = id_skill;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
   
}
