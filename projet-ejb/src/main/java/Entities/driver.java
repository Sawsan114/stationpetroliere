package Entities;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: driver
 *
 */
@Entity

public class driver implements Serializable {

	   
	@Id
	private Integer id_driver;
	private String Name;
	private String Lastname;
	private String Disponibility;
	private float salaire;
	private static final long serialVersionUID = 1L;
	@OneToMany
	private List<order> orders;
	@ManyToOne
	@JoinColumn(name="id_user", referencedColumnName="id_user" )
	private user user;
	
	public List<order> getOrder(){
		return orders;
	}
	public user getUser(){
		return user;
	}
	
	

	public driver() {
		super();
	}   
	public Integer getId() {
		return this.id_driver;
	}

	public void setId(Integer id) {
		this.id_driver = id;
	}   
	public float getSalaire() {
		return this.salaire;
	}

	public void setSalaire(float salaire) {
		this.salaire = salaire;
	}   
	public String getName() {
		return this.Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}   
	public String getLastname() {
		return this.Lastname;
	}

	public void setLastname(String Lastname) {
		this.Lastname = Lastname;
	}   
	public String getDisponibility() {
		return this.Disponibility;
	}

	public void setDisponibility(String Disponibility) {
		this.Disponibility = Disponibility;
	}
   
}
