package Entities;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: finance
 *
 */
@Entity

public class finance implements Serializable {

	@Id
	private int id_finance;
	private String type;
	private float turnover;
	private static final long serialVersionUID = 1L;


	public finance() {
		super();
	}   
	

	public int getId_finance() {
		return this.id_finance;
	}

	public void setId_finance(int id_finance) {
		this.id_finance = id_finance;
	}   
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}   
	public float getTurnover() {
		return this.turnover;
	}

	public void setTurnover(float turnover) {
		this.turnover = turnover;
	}
   
}
