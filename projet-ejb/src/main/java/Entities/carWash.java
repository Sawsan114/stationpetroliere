package Entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: carWash
 *
 */
@Entity

public class carWash implements Serializable {

	@Id
	private int id_wash;
	private static final long serialVersionUID = 1L;

	public carWash() {
		super();
	}   
	public int getId_wash() {
		return this.id_wash;
	}

	public void setId_wash(int id_wash) {
		this.id_wash = id_wash;
	}
   
}
