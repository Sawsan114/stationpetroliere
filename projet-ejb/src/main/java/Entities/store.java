package Entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: store
 *
 */
@Entity

public class store implements Serializable {

	@Id
	private int id_store;
	private int nbrprodsold;
	private static final long serialVersionUID = 1L;
	@OneToOne
	@JoinColumn(name="id_station")
	private station station;
	
	
	public station getStation(){
		return station;
	}


	public store() {
		super();
	}   
	public int getId_store() {
		return this.id_store;
	}

	public void setId_store(int id_store) {
		this.id_store = id_store;
	}   
	public int getNbrprodsold() {
		return this.nbrprodsold;
	}

	public void setNbrprodsold(int nbrprodsold) {
		this.nbrprodsold = nbrprodsold;
	}
   
}
