package Entities;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: order
 *
 */
@Entity

public class order implements Serializable {

	   
	@Id
	private int id_order;
	private String matricule;
	private Date deadline;
	private Date date_arrive;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name="id_driver", referencedColumnName="id_driver")
	private driver driver;
	
	@OneToOne
	@JoinColumn(name="id_fuel")
	private fuel fuel;

	public order() {
		super();
	}   
	public int getId_order() {
		return this.id_order;
	}
	public fuel getFuel() {
		return this.fuel;
	}

	public void setId_order(int id_order) {
		this.id_order = id_order;
	}   
	public String getMatricule() {
		return this.matricule;
	}

	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}   
	public Date getDeadline() {
		return this.deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}   
	public Date getDate_arrive() {
		return this.date_arrive;
	}

	public void setDate_arrive(Date date_arrive) {
		this.date_arrive = date_arrive;
	}
   
}
