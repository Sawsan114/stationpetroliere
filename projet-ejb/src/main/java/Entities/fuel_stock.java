package Entities;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: fuel_stock
 *
 */
@Entity

public class fuel_stock implements Serializable {

	   
	@Id
	private int id_stockF;
	private String place;
	private float quantity;
	private static final long serialVersionUID = 1L;
	@OneToOne
	@JoinColumn(name="id_Fuel")
	private fuel fuel;
	
	
	public fuel getFuel(){
		return fuel;
	}

	public fuel_stock() {
		super();
	}   
	public int getId_stockF() {
		return this.id_stockF;
	}

	public void setId_stockF(int id_stockF) {
		this.id_stockF = id_stockF;
	}   
	public String getPlace() {
		return this.place;
	}

	public void setPlace(String place) {
		this.place = place;
	}   
	public float getQuantity() {
		return this.quantity;
	}

	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}
   
}
