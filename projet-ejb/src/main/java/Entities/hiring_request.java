package Entities;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.*;


/**
 * Entity implementation class for Entity: hiring_request
 *
 */
@Entity

public class hiring_request implements Serializable {

	@Id
	private int id_request;
	private String description;
	private Date deadline;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name="id_user", referencedColumnName="id_user" )
	private user user;
	
	public user getUser(){
		return user;
	}

	public hiring_request() {
		super();
	}   
	public int getId_request() {
		return this.id_request;
	}

	public void setId_request(int id_request) {
		this.id_request = id_request;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}   
	public Date getDeadline() {
		return this.deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}
   
}
