
package Entities;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: product
 *
 */
@Entity

public class product implements Serializable {

	@Id
	private int id_product;
	private String name;
	private float initialPrice;
	private String picture;
	private String description;
	private static final long serialVersionUID = 1L;
	@ManyToMany
	private List<claims> claims;

	public product() {
		super();
	}   
	public int getId_product() {
		return this.id_product;
	}

	public void setId_product(int id_product) {
		this.id_product = id_product;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public float getInitialPrice() {
		return this.initialPrice;
	}

	public void setInitialPrice(float initialPrice) {
		this.initialPrice = initialPrice;
	}   
	public String getPicture() {
		return this.picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
   
}
