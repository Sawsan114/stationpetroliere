package Entities;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: user
 *
 */
@Entity

public class user implements Serializable {

	@Id
	private int id_user;
	private long cin;
	private String first_name;
	private String last_name;
	private String email;
	private long num_tlf;
	private String password;
	private String role;
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy="user")
	private List<appointment> appointments;
	@ManyToMany
	private List<claims> claims;
	@OneToMany(mappedBy="user")
	private List<hiring_request> hiring_requests;
	@OneToMany(mappedBy="user")
	private List<puits> puits;
	@OneToMany(mappedBy="user")
	private List<posts> posts;
	@ManyToOne
	@JoinColumn(name="id_station", referencedColumnName="id_station" )
	private station station;
	@OneToMany
	private List<driver> drivers;
	@OneToMany
	private List<truck> trucks;
	@OneToMany
	private List<pumpist> pumpists;
	@OneToMany
	private List<pump> pumps;
	@OneToMany
	private List<washWorker> washWorkers;

	
	
	public List<claims> getclaim(){
		return claims;
	}
	public List<driver> getDrivers(){
		return drivers;
	}
	public List<truck> getTruck(){
		return trucks;
	}
	public List<washWorker> getWashWorkers(){
		return washWorkers;
	}
	
	
	public station getStation(){
		return station;
	}
	
	
	public List<posts> getPost(){
		return posts;
	}
	
	
	public List<hiring_request> getHiring_request(){
		return hiring_requests;
	}
	
	public List<appointment> getAppointment(){
		return appointments;
	}

	public user() {
		super();
	}   
	public int getId_user() {
		return this.id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}   
	public long getCin() {
		return this.cin;
	}

	public void setCin(long cin) {
		this.cin = cin;
	}   
	public String getFirst_name() {
		return this.first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}   
	public String getLast_name() {
		return this.last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}   
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}   
	public long getNum_tlf() {
		return this.num_tlf;
	}

	public void setNum_tlf(long num_tlf) {
		this.num_tlf = num_tlf;
	}   
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}   
	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}
   
}
