package Entities;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: puits
 *
 */
@Entity

public class puits implements Serializable {

	@Id
	private int id;
	private String location;
	private String forage;
	private float estimation;
	private int quantity;
	private Date date_explorer;
	private static final long serialVersionUID = 1L;
	@OneToOne
	@JoinColumn(name="id_fuel")
	private fuel fuel;
	@OneToOne
	@JoinColumn(name="id_finance")
	private finance finance;
	@ManyToOne
	@JoinColumn(name="id_user", referencedColumnName="id_user" )
	private user user;
	
	
	public fuel getFuel(){
		return fuel;
	}
	
	public finance getFinance(){
		return finance;
	}

	public puits() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}   
	public String getForage() {
		return this.forage;
	}

	public void setForage(String forage) {
		this.forage = forage;
	}   
	public float getEstimation() {
		return this.estimation;
	}

	public void setEstimation(float estimation) {
		this.estimation = estimation;
	}   
	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}   
	public Date getDate_explorer() {
		return this.date_explorer;
	}

	public void setDate_explorer(Date date_explorer) {
		this.date_explorer = date_explorer;
	}
   
}
