package Entities;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;



/**
 * Entity implementation class for Entity: claims
 *
 */
@Entity

public class claims implements Serializable {

	@Id
	private int id_claim;
	private String object;
	private String msg;
	private static final long serialVersionUID = 1L;
	@ManyToMany
	@JoinTable(name="claims_user", joinColumns=@JoinColumn(name="id_claim"), inverseJoinColumns=@JoinColumn(name="id_user"))
	private List<user> users;
	
	
	public List<user> getUser(){
		return users;
	}

	public claims() {
		super();
	}   
	public int getId_claim() {
		return this.id_claim;
	}

	public void setId_claim(int id_claim) {
		this.id_claim = id_claim;
	}   
	public String getObject() {
		return this.object;
	}

	public void setObject(String object) {
		this.object = object;
	}   
	public String getMsg() {
		return this.msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
   
}
