package Entities;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: appointment
 *
 */
@Entity
@Table(name="appointment")

public class appointment implements Serializable {

	@Id
	private int id_appoinment;
	private String car_type;
	private float wash_price;
	private Date wash_date;
	private String wash_type;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name="id_user", referencedColumnName="id_user" )
	private user user;
	@ManyToOne
	@JoinColumn(name="id_worker", referencedColumnName="id_worker" )
	private washWorker washWorker;
	//private carWash carWash;
	
	
	public washWorker getWashWorker(){
		return washWorker;
	}
	
	
	public user getUser(){
		return user;
	}

	public appointment() {
		super();
	}   
	public int getId_appoinment() {
		return this.id_appoinment;
	}

	public void setId_appoinment(int id_appoinment) {
		this.id_appoinment = id_appoinment;
	}   
	public String getCar_type() {
		return this.car_type;
	}

	public void setCar_type(String car_type) {
		this.car_type = car_type;
	}   
	public float getWash_price() {
		return this.wash_price;
	}

	public void setWash_price(float wash_price) {
		this.wash_price = wash_price;
	}   
	public Date getWash_date() {
		return this.wash_date;
	}

	public void setWash_date(Date wash_date) {
		this.wash_date = wash_date;
	}   
	public String getWash_type() {
		return this.wash_type;
	}

	public void setWash_type(String wash_type) {
		this.wash_type = wash_type;
	}
   
}
