package Entities;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: pumpist
 *
 */
@Entity

public class pumpist implements Serializable {

	@Id
	private int id_pumpist;
	private long Cin;
	private String lastName;
	private String firstName;
	private int Disponibility;
	private float salary;
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name="id_user", referencedColumnName="id_user" )
	private user user;
	
	public pumpist() {
		super();
	}   
	public int getId_pumpist() {
		return this.id_pumpist;
	}

	public void setId_pumpist(int id_pumpist) {
		this.id_pumpist = id_pumpist;
	}   
	public long getCin() {
		return this.Cin;
	}

	public void setCin(long Cin) {
		this.Cin = Cin;
	}   
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}   
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}   
	public int getDisponibility() {
		return this.Disponibility;
	}

	public void setDisponibility(int Disponibility) {
		this.Disponibility = Disponibility;
	}   
	public float getSalary() {
		return this.salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}
   
}
