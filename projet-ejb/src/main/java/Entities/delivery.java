package Entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: delivery
 *
 */
@Entity

public class delivery implements Serializable {

	@Id
	private int id;
	private float quantity;
	private float price;
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name="id_pump", referencedColumnName="id_pump" )
	private pump pump;

	public delivery() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public float getQuantity() {
		return this.quantity;
	}

	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}   
	public float getPrice() {
		return this.price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
   
}
