package Entities;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: station
 *
 */
@Entity

public class station implements Serializable {

	@Id
	private int id_station;
	private String place;
	private static final long serialVersionUID = 1L;
	@OneToOne(mappedBy="station")
	private stock stock;
	@OneToOne(mappedBy="station")
	private store store;
	@OneToMany
	private List<pump> pumps;
	@OneToMany
	private List<pumpist> pumpists;
	@OneToMany
	private List<washWorker> washworkers;
	@OneToMany
	private List<user> users;
	
	
	public List<pump> getPump(){
		return pumps;
	}
	
	public List<pumpist> getPumpist(){
		return pumpists;
	}
	
	public List<washWorker> getWashWorker(){
		return washworkers;
	}
	
	
	
	public stock getStock(){
		return stock;
	}
	
	public store getStore(){
		return store;
	}
	
	
	

	public station() {
		super();
	}   
	public int getId_station() {
		return this.id_station;
	}

	public void setId_station(int id_station) {
		this.id_station = id_station;
	}   
	public String getPlace() {
		return this.place;
	}

	public void setPlace(String place) {
		this.place = place;
	}
   
}
