package Entities;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: fuel
 *
 */
@Entity

public class fuel implements Serializable {

	@Id
	private int id_fuel;
	private String type;
	private float quantity;
	private float price_unit;
	private static final long serialVersionUID = 1L;


	public fuel() {
		super();
	}   
	public int getId_fuel() {
		return this.id_fuel;
	}

	public void setId_fuel(int id_fuel) {
		this.id_fuel = id_fuel;
	}   
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}   
	public float getQuantity() {
		return this.quantity;
	}

	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}   
	public float getPrice_unit() {
		return this.price_unit;
	}

	public void setPrice_unit(float price_unit) {
		this.price_unit = price_unit;
	}
   
}
