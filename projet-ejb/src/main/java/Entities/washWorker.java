package Entities;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: washWorker
 *
 */
@Entity

public class washWorker implements Serializable {

	
	@Id
	private int id_worker;
	private String firstname;
	private String lastname;
	private int carNumber;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name="id_user", referencedColumnName="id_user" )
	private user user;
	
	@OneToMany
	private List<appointment> appointments;
	
	public user getUser(){
		return user;
	}
	
	public List<appointment> getAppointment(){
		return appointments;
	}
	

	public washWorker() {
		super();
	}   
	public int getId_worker() {
		return this.id_worker;
	}

	public void setId_worker(int id_worker) {
		this.id_worker = id_worker;
	}   
	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}   
	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}   
	public int getCarNumber() {
		return this.carNumber;
	}

	public void setCarNumber(int carNumber) {
		this.carNumber = carNumber;
	}
   
}
