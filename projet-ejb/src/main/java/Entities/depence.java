package Entities;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: depence
 *
 */
@Entity

public class depence implements Serializable {

	   
	@Id
	private Integer id_depence;
	private String Type;
	private static final long serialVersionUID = 1L;
	@OneToOne
	@JoinColumn(name="id_truck")
	private truck truck;

	@OneToOne
	@JoinColumn(name="id_finance")
	private finance finance;
	
	public depence() {
		super();
	}   
	public Integer getId() {
		return this.id_depence;
	}

	public void setId(Integer id) {
		this.id_depence = id;
	}   
	public String getType() {
		return this.Type;
	}

	public void setType(String Type) {
		this.Type = Type;
	}
   
}
