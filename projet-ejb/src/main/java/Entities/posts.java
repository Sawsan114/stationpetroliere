package Entities;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;

import javax.persistence.*;


/**
 * Entity implementation class for Entity: posts
 *
 */
@Entity

public class posts implements Serializable {

	@Id
	private int id_post;
	private String name;
	private String description;
	private String type;
	private Date date_post;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name="id_user", referencedColumnName="id_user" )
	private user user;
	
	
	
	public user getUser(){
		return user;
	}


	public posts() {
		super();
	}   
	public int getId_post() {
		return this.id_post;
	}

	public void setId_post(int id_post) {
		this.id_post = id_post;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}   
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}   
	public Date getDate_post() {
		return this.date_post;
	}

	public void setDate_post(Date date_post) {
		this.date_post = date_post;
	}
   
}
