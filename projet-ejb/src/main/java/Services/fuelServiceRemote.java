package Services;

import java.util.List;

import javax.ejb.Remote;

import Entities.fuel;

@Remote
public interface fuelServiceRemote {
	public int addFuel(fuel f);
	public void removeFuel(int id);
	public void updateFuel(fuel fuelNewValues, int id);
	public fuel findFuelById(int id);
	public List<fuel> findAllFuels();

}
