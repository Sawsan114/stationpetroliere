package Services;

import java.util.List;

import javax.ejb.Remote;

import Entities.hiring_request;

@Remote
public interface hiring_requestServiceRemote {

	public int addHiring_request(hiring_request h);
	public void removeHiring_request(int id);
	public void updateRequest(hiring_request requestNewValues, int id);
	public hiring_request findrequestsById(int id);
	public List<hiring_request> findAllrequests();
}
