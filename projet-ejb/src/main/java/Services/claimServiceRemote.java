package Services;

import java.util.List;

import javax.ejb.Remote;

import Entities.claims;

@Remote
public interface claimServiceRemote {
	public int addClaim(claims c);
	public void removeClaim(int id);
	public List<claims> findAllclaims();
	public claims findClaimById(int id);
	public void updateClaim(claims claimNewValues, int id);

}
