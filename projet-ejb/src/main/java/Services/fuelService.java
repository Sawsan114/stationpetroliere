package Services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

import Entities.fuel;

/**
 * Session Bean implementation class fuelService
 */
@Stateless
@LocalBean
public class fuelService implements fuelServiceRemote, fuelServiceLocal {

    /**
     * Default constructor. 
     */
	EntityManager em;
    public fuelService() {
        // TODO Auto-generated constructor stub
    }
    
	@Override
	public int addFuel(fuel f)
	{
		em.persist(f);
		return f.getId_fuel();
	}

	@Override
	public void removeFuel(int id)
	{
		em.remove(em.find(fuel.class, id));
	}

	@Override
	public void updateFuel(fuel fuelNewValues, int id)
	{
		fuel f = em.find(fuel.class, id);
		f.setType(fuelNewValues.getType());
		f.setQuantity(fuelNewValues.getQuantity());
		f.setPrice_unit(fuelNewValues.getPrice_unit());
	}

	@Override
	public fuel findFuelById(int id)
	{
		fuel f = em.find(fuel.class, id);
		return f;
	}

	@Override
	public List<fuel> findAllFuels()
	{
		List<fuel> f = em.createQuery("from fuel", fuel.class).getResultList();
		return f;
	} 

}
