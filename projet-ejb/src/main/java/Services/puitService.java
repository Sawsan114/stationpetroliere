package Services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import Entities.puits;


/**
 * Session Bean implementation class puitService
 */
@Stateless
@LocalBean
public class puitService implements puitServiceRemote, puitServiceLocal {

    /**
     * Default constructor. 
     */
	EntityManager em;
    public puitService() {
        // TODO Auto-generated constructor stub
    }
    @Override
	public void addPuit(puits p) {
		em.persist(p);
	}
	
    @Override
	public List<puits> getAllPuits(){
		List<puits> f = em.createQuery("from puits", puits.class).getResultList();
		return f;
	}
	
	@Override
	public void deletePuit(String location){
		em.remove(em.find(puits.class, location));
	}

}
