package Services;

import java.util.List;

import javax.ejb.Remote;

import Entities.fuel;
import Entities.posts;

@Remote
public interface postServiceRemote {
	public int addPost(posts p);
	public void removePost(int id);
	public void updateFuel(fuel fuelNewValues, int id);
	public posts findPostById(int id);
	public List<posts> findAllPosts();

}
