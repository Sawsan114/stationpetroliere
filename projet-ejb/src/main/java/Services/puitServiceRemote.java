package Services;

import java.util.List;

import javax.ejb.Remote;

import Entities.puits;

@Remote
public interface puitServiceRemote {
	public void addPuit(puits p);
	public List<puits> getAllPuits();
	public void deletePuit(String location);

}
