package Services;

import java.util.List;


import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

import Entities.claims;

/**
 * Session Bean implementation class claimService
 */
@Stateless
@LocalBean
public class claimService implements claimServiceRemote, claimServiceLocal {

    /**
     * Default constructor. 
     */
	EntityManager em;
    public claimService() {
        // TODO Auto-generated constructor stub
    }
	@Override
	public int addClaim(claims c)
	{
		em.persist(c);
		return c.getId_claim();
	}

	@Override
	public void removeClaim(int id)
	{
		em.remove(em.find(claims.class, id));
	}

	@Override
	public void updateClaim(claims claimNewValues, int id)
	{
		claims c = em.find(claims.class, id);
		c.setObject(claimNewValues.getObject());
		c.setMsg(claimNewValues.getMsg());
	}

	@Override
	public claims findClaimById(int id)
	{
		claims c = em.find(claims.class, id);
		return c;
	}

	@Override
	public List<claims> findAllclaims()
	{
		List<claims> f = em.createQuery("from claims", claims.class).getResultList();
		return f;
	} 

}
