package Services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

import Entities.fuel;
import Entities.posts;

/**
 * Session Bean implementation class postService
 */
@Stateless
@LocalBean
public class postService implements postServiceRemote, postServiceLocal {

    /**
     * Default constructor. 
     */
	EntityManager em;
    public postService() {
        // TODO Auto-generated constructor stub
    }
	@Override
	public int addPost(posts p)
	{
		em.persist(p);
		return p.getId_post();
	}

	@Override
	public void removePost(int id)
	{
		em.remove(em.find(posts.class, id));
	}

	@Override
	public void updateFuel(fuel fuelNewValues, int id)
	{
		fuel f = em.find(fuel.class, id);
		f.setType(fuelNewValues.getType());
		f.setQuantity(fuelNewValues.getQuantity());
		f.setPrice_unit(fuelNewValues.getPrice_unit());
	}

	@Override
	public posts findPostById(int id)
	{
		posts f = em.find(posts.class, id);
		return f;
	}

	@Override
	public List<posts> findAllPosts()
	{
		List<posts> f = em.createQuery("from posts", posts.class).getResultList();
		return f;
	} 

}
