package Services;

import java.util.List;


import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import Entities.hiring_request;

/**
 * Session Bean implementation class hiring_requestService
 */
@Stateless
@LocalBean
public class hiring_requestService implements hiring_requestServiceRemote, hiring_requestServiceLocal {

    /**
     * Default constructor. 
     */
	EntityManager em;
    public hiring_requestService() {
        // TODO Auto-generated constructor stub
    }
	@Override
	public int addHiring_request(hiring_request h)
	{
		em.persist(h);
		return h.getId_request();
	}

	@Override
	public void removeHiring_request(int id)
	{
		em.remove(em.find(hiring_request.class, id));
	}

	@Override
	public void updateRequest(hiring_request requestNewValues, int id)
	{
		hiring_request c = em.find(hiring_request.class, id);
		c.setDescription(requestNewValues.getDescription());
		c.setDeadline(requestNewValues.getDeadline());
	}

	@Override
	public hiring_request findrequestsById(int id)
	{
		hiring_request c = em.find(hiring_request.class, id);
		return c;
	}

	@Override
	public List<hiring_request> findAllrequests()
	{
		List<hiring_request> f = em.createQuery("from hiring_requests", hiring_request.class).getResultList();
		return f;
	} 

}
